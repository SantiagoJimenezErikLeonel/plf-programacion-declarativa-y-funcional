## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

Cuadro sinoptico de [Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c66b1111f54718b4911), [Programación Declarativa. Orientaciones y pautas para el estudio](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b) y [Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

Por: **Julio Gonzalo Arroyo** profesor del Departamento Lenguajes y Sistemas Informáticos, UNED

**Ana Isabel Ventureira Pedrosa** redactora - locutora, CEMAV, UNED

```plantuml
@startmindmap
header
07 de octubre del 2021
endheader
title Cuadro Sinóptico del resumen sobre Programación Declarativa. Orientaciones y pautas para el estudio."
legend right
  Elaborado por:
  Erik Leonel
  Santiago Jimenez
endlegend
<style>
mindmapDiagram {
  Linecolor blue
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .inicio {
    BackgroundColor #33DDFF
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }  
.green2 {
    BackgroundColor lightgreen
 RoundCorner 10
  FontSize 18
  }
}
</style>


* **Programación Declarativa**<<green2>>

**_ Surge
*** Reacción a problemas de programación clasica 
****_ Como ejemplo
*****  C
***** Phyton
***** Pascal

**_ Se basa en
*** Proposiciones y afirmaciones
*** Funciones o expresiones 
**** **Puramente matemáticas**
***_ No existe
**** Asignación de variables
*****_ Que mantendrá hasta 
****** El programa termine de ejecutarse 
****** Hasta que se le asigne un nuevo valor

**** Efectos secundarios
***_ Existe
**** Transparencia referencial
*****_ Se refiere 
****** Determinar el resultado
*******_ Solo mirando
******** Los valores de sus argumentos


**_ Es un
*** Paradigma de programación <<rose>>
**** Describe el resultado final deseado
***** Estilo de programación de software
**** Detalles de cálculo
****_ Trabaja mediante de
***** Funciones del tipo de órdenes

****_ Como ejemplo

***** Prolog
******_ Responden preguntas sobre 
******* El tema del cual tienen conocimiento

***** Lisp
******_ Muestra la información
******* Listas estructuradas que se pueden gestionar

***** Haskell
******_ predicen las búsquedas
******* De Google o Facebook 

***** Miranda
***** Erlang

** Ventajas <<inicio>>
*** Programas más cortos
****_ permite 
***** Ser razonados matemáticamente
*** Fácil de depurar
****_ optimiza
*****  Rendimiento de los programas
*** Sintaxis abreviada y abstracta
****_ Son
***** Fiables
***** Expresivos

** Desventajas <<inicio>>
*** Más complejo
****_ Está restringida
***** Al subconjunto de problemas
******_ Para
******* El intérprete o compilador fue diseñado.

*** **Difícil de comprender**
****_  Es necesaria
***** Interpretación extra


***_ Basado en 
**** Una forma de pensar
***** No habitual en las personas
******_ Se evaluan
******* Todas las consecuencias de todas las declaraciones

**_ Contraparte de
*** **Declaración imperativa** <<rose>>
**** Programación más antiguo
****_ Tipos
***** Estructurados<<greencute>>
******_ Es una
******* Teoría orientada a mejorar la claridad

***** Procedurales<<greencute>>
******_ Divide las tareas
******* A tareas parciales más pequeñas 
********_ Describen en el código por separado.

***** Orientado a objetos<<greencute>>
**** Separar del modelo a la memoria
*****_ Fomenta 
****** La reutilización
****** Ampliación del código

 
** Tipos <<inicio>>

*** **Programación lógica** <<rose>>
****_ Parte de
***** Lógica de predicados de primer orden
******_ Permite s
******* Ser más declarativos	
***** Instrucciones, condiciones y pasos
*****_ Existen
****** Relaciones entre objetos
*******_ Abarcan
******** Operaciones
********  Resultados 
******** Suposiciones 
****** No hay un orden
*****_ Ejemplo
****** Definir relacion factoriales
*******_ Ocupa
******** Un Modelo factorial definido


*** **Programación funcional**<<rose>>
****_ Utiliza
***** Lenguaje Matematico
***** Evaluación perezosa
******_ Evita repetir 
******* La evaluación
***** Evaluación de ecuaciones
******_ Son
******* Estrictamente necesarias
***** Define tipos de datos infinitos
******_ Por ejemplo 
******* Fibonacci
****** Útil si se evalúa o computa la lista
****_ **Desarrollo de**
***** Aplicaciones técnicas y matemáticas
******_ Como
******* Tareas matemáticas
***** Inteligencia Artificial (IA)
******_ Sistemas
******* Altamente seguros
*******_ Ejemplo de 
******** Prolog
******** Haskell
********* Algoritmos de búsqueda
***** Compiladores y analizadores
***** Algoritmos
******_ Son
******* Conjunto de instrucciones
******** Definidas 
******** Ordenadas 
******** Acotadas









@endmindmap
```

## Lenguaje de Programación Funcional (2015)

Cuadro sinoptico de [Lenguaje de Programación Funcional](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)

Profundizamos en las características de la programación funcional, construcciones fundamentales del lenguaje y en los paradigmas de programación. Repasamos los contenidos del libro Teoría de los Lenguajes de Programación en el que se recoge amplia información sobre su evolución histórica, fundamentos teóricos, los distintos usos, etc. para poder apreciar sus características diferentes, entender la influencia que ejercen las arquitecturas y aplicaciones de los ordenadores sobre el diseño de los LP y evitar defectos de diseño de programas.

Por: **Julio Gonzalo Arroyo** profesor del Departamento Lenguajes y Sistemas Informáticos, UNED

**Ana Isabel Ventureira Pedrosa** redactora - locutora, CEMAV, UNED


```plantuml
@startmindmap


header
07 de octubre del 2021
endheader
title Cuadro Sinóptico del resumen sobre "Lenguaje de Programación Funcional"
legend right
  Elaborado por:
  .     Erik Leonel
  Santiago Jimenez
endlegend


<style>
mindmapDiagram {
  Linecolor green

  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  RoundCorner 10
  }
  .inicio {
    BackgroundColor lightblue
  }
  .pink {
    BackgroundColor #F5CBA7
  }
 .yellow {
    BackgroundColor #F7F9F9
  }
 .greencute {
    BackgroundColor #A3E4D7
  }
.green2 {
    BackgroundColor lightgreen
 RoundCorner 10
  FontSize 18
  }
}
</style>


* **Programación Funcional** <<green2>>

** **Modelo de computación**
*** Métodos fundamentales de programación
****_ Es una
***** Subcategoría de la estrategia declarativa
***_ Se remontan en 
**** La década de 1930


** **Surge** <<rose>>
*** **Von Newman** <<green>>
****_ Propuso
***** La adopción del bit
******_ Como 
******* Unidad de medida de la memoria 
********_ Para
********* Las computadoras

****_ Desarrolló el concepto
***** **Bits de paridad**
******_ Para
******* Deteccion de errores,

**** Arquitectura de J Von
***** La Memoria principal
******_ Gestiona
******* Instrucciones ejecutandose 
******* Instruccion de asignación

**** Lenguaje Imperativo
*****_ Donde
****** Las instrucciones se ejecutan unas tras otras
****** Objetos que interactuan entre si

*** **Lamda cálculo** <<green>>
****_ Es una
***** Notación formal
******_ Permite 
******* Expresar funciones computables 
***** Conceptos matematicos
*****_ Se expresa 
****** Expresiones lambda
*******_ Representan
******** Variables dentro de una función
****** Términos lambda
*******_ Representan
******** Variables dentro de una función
***** Logica computacional
******_ es la misma a
******* Lógica matemática
********_ Aplicada a
********* Contexto de la computación.

****_ Investiga 
***** La naturaleza de las funciones
***** La computabilidad
******_ Por ejemplo
******* **Lenguajes establecidos**<<rose>>
******** LISP
******** ML
******** Haskell
******** OCaml
******** F#
******** rlang
******** Clojure
******** Scala


** **Bases** <<rose>>
***_ De las
**** Funciones matematicas
*****_ Son
****** Funciones que se definen y después se aplican
*****_ Ofrece 
****** Un alto grado de abstracción



** **Desventajas** <<rose>>
*** Reducción en el rendimiento
****_ Al combinar
***** Valores inmutables
***** Recursividad 
******_ En lugar de 
******* Bucles
*** Asignación no existe 
*** No existen los bucles como tal
****_ Como Factorial
****_ Como Fibonacci


** **Ventajas** <<rose>>
***_ Códigos 
**** Fácilmente verificable
**** Más preciso y más corto
*** El flujo lógico es claro
****_ Por lo cual
***** No se modifica implícitamente
***_ Admite
**** Concepto de evaluación diferida
*****_ Significa
****** El valor solo se evalúa
****** Almacena cuando sea necesario

*** Las funciones puras
****_  Debido a esto
***** No cambian ningún estado
****** Evita que cambien las variables
***** Dependen de la entrada
******_ Por lo cual
******* Evita cualquier dato externo



** **Caracteristicas** <<rose>>
*** El orden no es relevante
*** Todo gira en torno a funciones
**** Funciones de funciones
***_ Técnica de evaluaciónes
**** Evaluación Peresoza
***** Retrasa el cálculo de una expresión
**** Evaluacion no estricta
***** Modularidad de los programas\na través de la \nseparación de tareas 
**** Evaluacion estricta
***** Evalúa argumentos por \ncompleto a menos que \nrequieran evaluar \nla propia función
**** Memoizacion 
***** Evita recalcular resultados previamente obtenidos

**_ Se utiliza en
*** **Aplicaciones** <<rose>>
**** Fines académicos 
*****_ Como
****** Programacion escolar
****** Resolver sudoku
**** desarrollo de software comercial
*****_ Como
**** Programar videojuegos
*****_ Como ejemplo
****** Monadius
******* arcade scroller programado en Prog. Funcional
**** Aplicaciones técnicas y matemáticas
**** Inteligencia Artificial (IA)
**** Compiladores y analizadores
*****_ Desarrollar
****** Variedad de aplicaciones comerciales e industriales.
**** Algoritmos
*****_ Ejemplo 
****** WhatsApp utiliza Erlang
*******_ Para manipular
******** Datos de 1.500 millones de personas.

** Enfoque funcional
***_ Se puede colocar
**** Estado intermedio 
*****_ Entre 
****** Los estilos de programación




@endmindmap
```

